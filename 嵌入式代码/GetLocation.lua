--GPS相关的业务 返回高德系坐标 需要使用浮点固件
module(...,package.seeall)
require "gpsv3"

local GaoDe_Location = {} 
local Location = {} --坐标转化后存储位置

Location.lng = 0
Location.lat = 0
Location.Altitude = 0

GaoDe_Location.Longitude = 0
GaoDe_Location.Latitude = 0
GaoDe_Location.Altitude = 0
GaoDe_Location.CoordinateSystem = 2  --告诉平台，使用的是高德坐标系


--返回高德地图坐标系
function Get_GaoDe_Location()
    return GaoDe_Location
end

function _transformlat(lng, lat)
    local pi = 3.1415926535897932384626  --π
    local ee = 0.00669342162296594323  --扁率
	ret = -100.0 + 2.0 * lng + 3.0 * lat + 0.2 * lat * lat + 0.1 * lng * lat + 0.2 * math.sqrt(math.abs(lng))
	ret = ret + (20.0 * math.sin(6.0 * lng * pi) + 20.0 * math.sin(2.0 * lng * pi)) * 2.0 / 3.0
	ret = ret + (20.0 * math.sin(lat * pi) + 40.0 * math.sin(lat / 3.0 * pi)) * 2.0 / 3.0
	ret = ret + (160.0 * math.sin(lat / 12.0 * pi) + 320 * math.sin(lat * pi / 30.0)) * 2.0 / 3.0
	return ret
end
function _transformlng(lng, lat)
    local pi = 3.1415926535897932384626  --π
    local ee = 0.00669342162296594323  --扁率
	ret = 300.0 + lng + 2.0 * lat + 0.1 * lng * lng + 0.1 * lng * lat + 0.1 * math.sqrt(math.abs(lng))
	ret = ret + (20.0 * math.sin(6.0 * lng * pi) + 20.0 * math.sin(2.0 * lng * pi)) * 2.0 / 3.0
	ret = ret +  (20.0 * math.sin(lng * pi) + 40.0 * math.sin(lng / 3.0 * pi)) * 2.0 / 3.0
	ret = ret +  (150.0 * math.sin(lng / 12.0 * pi) + 300.0 * math.sin(lng / 30.0 * pi)) * 2.0 / 3.0
	return ret
end
--返回真实的位置信息，坐标规范符合高德地图
function Run_Tran()

	local pi = 3.1415926535897932384626  --π
    local ee = 0.00669342162296594323  --扁率
    local a = 6378245.0  -- 长半轴
	--WGS84转GCJ02(火星坐标系)
	--lng:WGS84坐标系的经度
    --lat:WGS84坐标系的纬度

    local lng = Location.lng
    local lat = Location.lat

	dlat = _transformlat(lng - 105.0, lat - 35.0)
	dlng = _transformlng(lng - 105.0, lat - 35.0)
	radlat = lat / 180.0 * pi
	magic = math.sin(radlat)
	magic = 1 - ee * magic * magic
	sqrtmagic = math.sqrt(magic)
	dlat = (dlat * 180.0) / ((a * (1 - ee)) / (magic * sqrtmagic) * pi)
	dlng = (dlng * 180.0) / (a / sqrtmagic * math.cos(radlat) * pi)
	mglat = lat + dlat
	mglng = lng + dlng
    
    Location.lng = mglng
    Location.lat = mglat

end
--基站定位回调函数 若GPS定位失败则基站定位信息填充
function getLocCb(result,lat,lng)
    if result==0 then
        if gpsv3.isFix() ~= true  then
            Location.lng = lng
            Location.lat = lat
            Location.Altitude = 0
            Run_Tran()
            GaoDe_Location = {}
            GaoDe_Location.lng = Location.lng
            GaoDe_Location.lat = Location.lat
            GaoDe_Location.LBS = true
            log.info("getLocCb","gpsv3.isFix() = False")
        else
            log.info("getLocCb","gpsv3.isFix() = True")
        end
    end
end
--打开基站定位
function OpenLBS()
    lbsLoc.request(getLocCb)
end
--GPS定位成功则处理GPS数据
local function GPS_OK()
    if gpsv3.isFix() == true then
        Location.lng,Location.lat = gpsv3.getDegLocation() --获取经纬度
        Location.Altitude = gpsv3.getAltitude() --获取海拔
        Run_Tran()
        GaoDe_Location = {}
        GaoDe_Location.Longitude = Location.lng
        GaoDe_Location.Latitude = Location.lat
        GaoDe_Location.Altitude = Location.Altitude
        GaoDe_Location.CoordinateSystem = 2  --告诉平台，使用的是高德坐标系
        GaoDe_Location.LBS = false
        log.info("GPS_OK","gpsv3.isFix() = True")
    else
        log.info("GPS_OK","gpsv3.isFix() = False")
    end
end


gpsv3.open(2,115200,20,false,true)
gpsv3.noLog(false)
sys.timerLoopStart(OpenLBS,25*1000) --57S刷新一次LBS定位
sys.subscribe("GPS_MSG_REPORT",GPS_OK) --GPS若定位成功则执行此函数


