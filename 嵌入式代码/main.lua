--GSM定位器模块安装到皮卡丘玩偶内部
--一旦通电，立即上报位置信息，三十秒一次。

--版本发布记录

--必须在这个位置定义PROJECT和VERSION变量
PROJECT = "Pikachu"
VERSION = "1.0.0"

require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE
--LOG_LEVEL = log.LOGLEVEL_ERROR
--LOG_LEVEL = log.LOGLEVEL_FATAL

--基站定位用
PRODUCT_KEY = "enfFQUSPW1IrCToRGhpkaI3fbZ3UKioz"


require "sys"
require "sim"
require "misc"
require "net"
require "pm"
require "lbsLoc"
require "netLed"
require"Config"
require "nvm"
require "ntp"

ntp.timeSync() -- 只同步1次时间

net.startQueryAll(30000, 30000)

netLed.setup(true,pio.P0_28)

nvm.init("Config.lua")  --初始化文件系统

require "GetLocation"
require "AliIOT"

--启动系统框架
sys.init(0, 0)
sys.run()



