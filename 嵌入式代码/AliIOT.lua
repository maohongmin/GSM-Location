--链接阿里云物联网套件

module(...,package.seeall)

require"aLiYun"
require"misc"
require"pm"
require"Config"
require"nvm"
require "net"
require "mqtt"
require "pins"
require "GetLocation"

--阿里云客户端是否处于连接状态
local sConnected

--库函数的要求，写的一个转换子函数
local function getDeviceName()
    return nvm.get("DeviceName")
end
--库函数的要求，写的一个转换子函数
local function getDeviceSecret()
    return nvm.get("DeviceSecret")
end
--上报各个属性 60S循环一次
local function Pub_Property()
    if sConnected then  --只有链接成功才执行上报任务
        --属性上报主题
        GeoLocation = GetLocation.Get_GaoDe_Location()
        local Propety_TOPIC = "/sys/"..nvm.get("ProductKey").."/"..getDeviceName().."/thing/event/property/post"
        local PayLoad = {}
        local Location = {}
        PayLoad.id = 1
        PayLoad.version = "1.0"
        PayLoad.method = "thing.event.property.post"
        local Params = {}
        Params.RSSI = net.getRssi()
        Params.BatteryLevel = misc.getVbatt()
        
        if GeoLocation.LBS then
            Location.lng = GeoLocation.lng
            Location.lat = GeoLocation.lat
            Params.LBS = Location
        else
            if GeoLocation.Longitude ~= 0 and GeoLocation.Latitude ~= 0 then
                Location.Longitude = GeoLocation.Longitude
                Location.Latitude = GeoLocation.Latitude
                Location.Altitude = GeoLocation.Altitude
                Location.CoordinateSystem = GeoLocation.CoordinateSystem
                Params.GeoLocation = Location
            end
        end

        PayLoad.params = Params
        PayLoad = json.encode(PayLoad)
        aLiYun.publish(Propety_TOPIC,PayLoad)   --上报阿里云属性  
    end
end
--- 连接结果的处理函数
local function connectCbFnc(result)
    sConnected = result
    if sConnected==true then
        Pub_Property()
    end
end
--开机初始化设备
function Init_Device()
    --配置阿里云的链接参数
    aLiYun.setMqtt(1,nil,60)
    aLiYun.setup(nvm.get("ProductKey"),nil,getDeviceName,getDeviceSecret)
    aLiYun.on("connect",connectCbFnc)
end

Init_Device() --初始化IOT参数
sys.timerLoopStart(Pub_Property,30*1000) --30S上报一次属性
require"aLiYunOta"
--如果利用阿里云OTA功能去下载升级合宙模块的新固件，默认的固件版本号格式为：_G.PROJECT.."_".._G.VERSION.."_"..sys.getcorever()，下载结束后，直接重启，则到此为止，


